package configs

// Config ...
type Config struct {
	Port    string `yaml:"port"`
	Tick    int    `yaml:"tick"`
	APILink string
	APIKey  string
}
