package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // pq lib
)

// Config ...
type Config struct {
	DBName     string
	DBHost     string
	DBPassword string
	DBPort     string
	DBUser     string
}

// NewPostgresDB ...
func NewPostgresDB(cfg *Config) (*sqlx.DB, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("user=%s dbname=%s password=%s port=%s host=%s sslmode=disable",
		cfg.DBUser, cfg.DBName, cfg.DBPassword, cfg.DBPort, cfg.DBHost))
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
