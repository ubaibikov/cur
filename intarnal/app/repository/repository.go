package repository

import "github.com/jmoiron/sqlx"

// Repository ...
type Repository struct {
	Currency
}

// NewRepository ...
func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Currency: NewCurrencyPostgres(db),
	}
}
