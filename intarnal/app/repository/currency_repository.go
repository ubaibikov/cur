package repository

import (
	"time"

	"bitbucket.org/ubaibikov/cur/intarnal/app/model"
	"github.com/jmoiron/sqlx"
)

// Currency ...
type Currency interface {
	Create(cur1, cur2 string) (int, error)
	UpdateCource(cur1, cur2, cource string) error
	GetAll() ([]model.Currency, error)
	Find(cur1, cur2 string) (*model.Currency, error)
}

// CurrencyPostgres ...
type CurrencyPostgres struct {
	db *sqlx.DB
}

// NewCurrencyPostgres ...
func NewCurrencyPostgres(db *sqlx.DB) *CurrencyPostgres {
	return &CurrencyPostgres{
		db: db,
	}
}

// Create ...
func (r *CurrencyPostgres) Create(cur1, cur2 string) (int, error) {
	var id int
	if err := r.db.QueryRow("insert into currency (cur1, cur2, updated_at) values($1,$2,$3) returning id",
		cur1, cur2, time.Now()).Scan(&id); err != nil {
		return 0, err
	}
	return id, nil
}

// UpdateCource ...
func (r *CurrencyPostgres) UpdateCource(cur1, cur2, cource string) error {
	if _, err := r.db.Exec(`update currency set cource=$1, updated_at=$2 
		where cur1=$3 and cur2=$4`, cource, time.Now(), cur1, cur2); err != nil {
		return err
	}
	return nil
}

// GetAll ...
func (r *CurrencyPostgres) GetAll() ([]model.Currency, error) {
	curs := []model.Currency{}

	if err := r.db.Select(&curs, "select * from currency"); err != nil {
		return nil, err
	}

	return curs, nil
}

// Find ...
func (r *CurrencyPostgres) Find(cur1, cur2 string) (*model.Currency, error) {
	var currency model.Currency
	if err := r.db.QueryRowx("select * from currency where cur1=$1 and cur2=$2",
		cur1, cur2).StructScan(&currency); err != nil {
		return nil, err
	}
	return &currency, nil
}
