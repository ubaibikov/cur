package handler

import (
	"bitbucket.org/ubaibikov/cur/intarnal/app/service"
	"github.com/gin-gonic/gin"
)

// Handler ....
type Handler struct {
	services *service.Service
}

// NewHandler ...
func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

// InitRoutes ...
func (h *Handler) InitRoutes() *gin.Engine {
	r := gin.Default()

	r.POST("/api/create", h.currencyCreate)
	r.GET("/api/convert", h.currencyConvert)

	return r
}
