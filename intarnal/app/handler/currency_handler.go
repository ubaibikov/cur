package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func (h *Handler) currencyCreate(c *gin.Context) {

	type createCurrencyForm struct {
		Cur1 string `json:"currency1" binding:"required"`
		Cur2 string `json:"currency2" binding:"required"`
	}

	var currency createCurrencyForm

	c.BindJSON(&currency)

	id, err := h.services.Currency.Create(currency.Cur1, currency.Cur2)
	if err != nil {
		logrus.Fatal("handler: currecnyCreate", err)
		c.JSON(http.StatusBadGateway, gin.H{
			"error_message": "Извините что-то не получилось",
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"success_message": "Валютная пара создана!",
		"id":              id,
	})
}

func (h *Handler) currencyConvert(c *gin.Context) {

	currencyFrom := c.Query("currencyFrom")
	currencyTo := c.Query("currencyTo")
	model, err := h.services.Currency.Find(currencyFrom, currencyTo)

	if err != nil {
		logrus.Fatal("handler: currencyConvert", err)
		c.JSON(http.StatusBadGateway, gin.H{
			"error_message": "Извините что-то не получилось",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success_message": "пеервод валют",
		"data":            model.Cource.String,
	})
}
