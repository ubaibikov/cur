package model

import "database/sql"

// Currency ...
type Currency struct {
	ID        int            `json:"id" db:"id"`
	Currency1 string         `json:"cur1" db:"cur1"`
	Currency2 string         `json:"cur2" db:"cur2"`
	Cource    sql.NullString `json:"cource" db:"cource"`
	UpdatedAt string         `json:"updated_at" db:"updated_at"`
}
