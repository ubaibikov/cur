package service

import "bitbucket.org/ubaibikov/cur/intarnal/app/repository"

// Service ....
type Service struct {
	Currency
}

// NewService ...
func NewService(repos *repository.Repository) *Service {
	return &Service{
		Currency: NewCurrencyService(repos.Currency),
	}
}
