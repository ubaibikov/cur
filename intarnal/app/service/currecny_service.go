package service

import (
	"bitbucket.org/ubaibikov/cur/intarnal/app/model"
	"bitbucket.org/ubaibikov/cur/intarnal/app/repository"
)

// Currency ...
type Currency interface {
	Create(cur1, cur2 string) (int, error)
	UpdateCource(cur1, cur2, cource string) error
	GetAll() ([]model.Currency, error)
	Find(cur1, cur2 string) (*model.Currency, error)
}

// CurrencyService ...
type CurrencyService struct {
	repo repository.Currency
}

// NewCurrencyService ...
func NewCurrencyService(repo repository.Currency) *CurrencyService {
	return &CurrencyService{
		repo: repo,
	}
}

// Create ...
func (s *CurrencyService) Create(cur1, cur2 string) (int, error) {
	return s.repo.Create(cur1, cur2)
}

// UpdateCource ...
func (s *CurrencyService) UpdateCource(cur1, cur2, cource string) error {
	return s.repo.UpdateCource(cur1, cur2, cource)
}

// GetAll ...
func (s *CurrencyService) GetAll() ([]model.Currency, error) {
	return s.repo.GetAll()
}

// Find ...
func (s *CurrencyService) Find(cur1, cur2 string) (*model.Currency, error) {
	return s.repo.Find(cur1, cur2)
}
