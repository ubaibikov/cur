package main

import (
	"context"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/ubaibikov/cur/configs"
	"bitbucket.org/ubaibikov/cur/intarnal/app/handler"
	"bitbucket.org/ubaibikov/cur/intarnal/app/repository"
	"bitbucket.org/ubaibikov/cur/intarnal/app/service"
	"bitbucket.org/ubaibikov/cur/pkg/usecase"
	"bitbucket.org/ubaibikov/cur/server"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var config []byte

const (
	configPath = "../configs/config.yml"
	envPath    = "../.env"
)

func init() {
	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		logrus.Fatal(err)
	}
	config = data

	err = godotenv.Load(envPath)
	if err != nil {
		logrus.Fatal(err)
	}
}

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))

	dbConfig := repository.Config{
		DBHost:     os.Getenv("DB_HOST"),
		DBName:     os.Getenv("DB_NAME"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBPort:     os.Getenv("DB_PORT"),
		DBUser:     os.Getenv("DB_USER"),
	}

	db, err := repository.NewPostgresDB(&dbConfig)
	if err != nil {
		logrus.Fatal(err)
	}
	defer db.Close()

	repository := repository.NewRepository(db)
	services := service.NewService(repository)
	handler := handler.NewHandler(services)

	var cnf configs.Config
	if err := yaml.Unmarshal(config, &cnf); err != nil {
		logrus.Fatal(err)
	}

	cnf.APIKey = os.Getenv("API_KEY")
	cnf.APILink = os.Getenv("API_LINK")

	rootCtx, rootCancel := context.WithCancel(context.Background())

	usecase := usecase.NewUseCase(services, cnf)
	go usecase.UpdateCurrecny(rootCtx)

	srv := new(server.Server)
	go func() {
		if err := srv.Run(cnf.Port, handler.InitRoutes()); err != nil {
			logrus.Fatal(err)
		}
	}()

	logrus.Info("app started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit //чтение из канала блокируем main

	rootCancel()

	if err := srv.Shutdown(rootCtx); err != nil {
		logrus.Errorf("server shutted down %s", err.Error())
	}

	if err := db.Close(); err != nil {
		logrus.Errorf("error in db.Close: %s", err.Error())
	}

}
