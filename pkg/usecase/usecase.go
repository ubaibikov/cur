package usecase

import (
	"bitbucket.org/ubaibikov/cur/configs"
	"bitbucket.org/ubaibikov/cur/intarnal/app/service"
)

// UseCase ...
type UseCase struct {
	CurrencyAPI
}

// NewUseCase ...
func NewUseCase(serv *service.Service, cnf configs.Config) *UseCase {
	return &UseCase{
		CurrencyAPI: NewCurrenencyCase(serv.Currency, cnf),
	}
}
