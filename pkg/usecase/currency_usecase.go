package usecase

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/ubaibikov/cur/configs"
	"bitbucket.org/ubaibikov/cur/intarnal/app/service"
	"github.com/sirupsen/logrus"
)

// CurrencyAPI ...
type CurrencyAPI interface {
	UpdateCurrecny(ctx context.Context)
}

// CurrecnyCase ...
type CurrecnyCase struct {
	serv service.Currency
	cnf  configs.Config
}

// NewCurrenencyCase ...
func NewCurrenencyCase(serv service.Currency, cnf configs.Config) *CurrecnyCase {
	return &CurrecnyCase{
		serv: serv,
		cnf:  cnf,
	}
}

// UpdateCurrecny ...
func (c *CurrecnyCase) UpdateCurrecny(parent context.Context) {
	for {
		select {
		case <-parent.Done():
			logrus.Info("usecase: currecny close")
			return
		case <-time.After(time.Duration(c.cnf.Tick) * time.Second):
			logrus.Info("update")
			// TODO:: сервис временно заблочен
			// if err := updateCurrencies(c.serv, c.cnf.APILink, c.cnf.APIKey); err != nil {
			// 	logrus.Fatal(err)
			// }
		}
	}
}

type freeCurConv map[string]float64

func updateCurrencies(currency service.Currency, APILink, APIKey string) error {
	curs, err := currency.GetAll()
	if err != nil {
		log.Fatal(err)
	}

	for _, data := range curs {

		pairs := data.Currency1 + "_" + data.Currency2

		client := http.Client{}
		request, _ := http.NewRequest("GET", APILink+pairs+"&compact=ultra&apiKey="+APIKey, nil)
		resp, _ := client.Do(request)

		var result freeCurConv

		if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
			return err
		}

		cource := fmt.Sprintf("%f", result[pairs])

		if err = currency.UpdateCource(data.Currency1, data.Currency2, cource); err != nil {
			return err
		}

		logrus.Infof("usecase: currecny method:UpdateCurrecny pairs: %s", pairs)
	}
	return nil
}
